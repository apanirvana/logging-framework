package ke.co.apollo.gis.logging.async;

import ke.co.apollo.gis.logging.config.LoggingProperties;
import ke.co.apollo.gis.logging.util.CorrelationIdUtil;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.MDC;

public abstract class BaseInterceptor {

    protected LoggingProperties loggingProperties;

    public BaseInterceptor(LoggingProperties loggingProperties) {
        this.loggingProperties = loggingProperties;
    }

    /**
     * Pointcut to match methods within package ke.co.apollo.
     */
    @Pointcut("execution(* ke.co.apollo..*(..))")
    public void targetScheduledMethods() {
    }

    public abstract void intercept(ProceedingJoinPoint proceedingJoinPoint) throws Throwable;

    protected void addMdcContext(ProceedingJoinPoint joinPoint) throws Throwable {
        MDC.put(this.loggingProperties.getMdcKey(),
                CorrelationIdUtil.getCorrelationId(this.loggingProperties.getMdcKey()));
        try {
            joinPoint.proceed();
        } finally {
            MDC.clear();
        }
    }
}
