package ke.co.apollo.gis.logging.filter;

import ke.co.apollo.gis.logging.util.CorrelationIdUtil;
import ke.co.apollo.gis.logging.config.LoggingProperties;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.MDC;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Filter to add unique CorrelationId to {@link MDC} for each incoming request.
 */
public class CorrelationFilter implements Filter {

    private LoggingProperties loggingProperties;

    /**
     * Parameterized Constructor.
     *
     * @param loggingProperties - Logging properties bean.
     */
    public CorrelationFilter(final LoggingProperties loggingProperties) {
        this.loggingProperties = loggingProperties;
    }

    /**
     * Initialization.
     *
     * @param filterConfig - Filter config.
     * @throws ServletException - {@link ServletException}
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    /**
     * Filter method to add MDC logic for each request.
     *
     * @param servletRequest  - http request
     * @param servletResponse - http response
     * @param filterChain     - filter chain
     * @throws IOException      - I/O Exception
     * @throws ServletException - Servlet exception
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws
            IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        String correlationId = request.getHeader(this.loggingProperties.getHeaderName());
        if (StringUtils.isBlank(correlationId)) {
            correlationId = CorrelationIdUtil.generateId();
        }
        try {
            MDC.put(this.loggingProperties.getMdcKey(), correlationId);
            filterChain.doFilter(servletRequest, servletResponse);
        } finally {
            ((HttpServletResponse) servletResponse)
                    .addHeader(this.loggingProperties.getHeaderName(), correlationId);
            MDC.clear();
        }
    }

    /**
     * Destroy method to clear resources.
     */
    @Override
    public void destroy() {
    }
}
