package ke.co.apollo.gis.logging;

import ke.co.apollo.gis.logging.config.CorrelationConfig;
import ke.co.apollo.gis.logging.config.LoggingProperties;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Import;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>Enables logging requests with correlation ID to correlate all logs in a single request. Initializes all beans
 * required for achieving this functionality. Enabling this library, adds correlation ID for each request execution
 * logs. Supported functionalities include HTTP API calls, RestTemplate calls, @Async, @Scheduled, @SqsListener.</p>
 *
 * <p><b>Below defaults are used for configuration and can be overwritten.</b></p>
 *
 * <ul>
 *   <li>logging.pattern.console=%d{dd-MM-yyyy HH:mm:ss.SSS} [%thread] %-5level %X{RequestId} %logger{36} - %msg%n</li>
 *   <li>correlation.logging.headerName=X-Correlation-Id</li>
 *   <li>correlation.logging.mdcKey=RequestId</li>
 *   <li>correlation.logging.enableScheduled=false</li>
 *   <li>correlation.logging.enableSqsListener=false</li>
 * </ul>
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Import(CorrelationConfig.class)
@ConfigurationPropertiesScan("ke.co.apollo.gis.logging.config")
@EnableConfigurationProperties(LoggingProperties.class)
@Documented
public @interface EnableCorrelationLogging {
}
