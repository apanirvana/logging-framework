package ke.co.apollo.gis.logging.rest;

import ke.co.apollo.gis.logging.config.LoggingProperties;
import ke.co.apollo.gis.logging.util.CorrelationIdUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;

/**
 * Request interceptor for http calls using {@link org.springframework.web.client.RestTemplate}
 */
public class RestApiInterceptor implements ClientHttpRequestInterceptor {

    private LoggingProperties loggingProperties;

    /**
     * Parameterized Constructor
     *
     * @param loggingProperties Properties
     */
    public RestApiInterceptor(final LoggingProperties loggingProperties) {
        this.loggingProperties = loggingProperties;
    }

    /**
     * Handler to perform updates/operation before executing http request.
     *
     * @param httpRequest   - request
     * @param bytes         - request body
     * @param httpExecution - http execution
     * @return - ClientHttpResponse - http response
     * @throws IOException - I/O Exception
     */
    @Override
    public ClientHttpResponse intercept(HttpRequest httpRequest, byte[] bytes, ClientHttpRequestExecution httpExecution) throws
            IOException {
        String correlationId =
                CorrelationIdUtil.getCorrelationId(this.loggingProperties.getMdcKey());
        httpRequest.getHeaders().add(this.loggingProperties.getHeaderName(), correlationId);
        ClientHttpResponse res =  httpExecution.execute(httpRequest, bytes);
        if(StringUtils.isBlank(res.getHeaders().getFirst(this.loggingProperties.getHeaderName()))) {
            res.getHeaders().add(this.loggingProperties.getHeaderName(), correlationId);
        }
        return res;
    }
}
