package ke.co.apollo.gis.logging.config;

import ke.co.apollo.gis.logging.util.LogConstants;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Logging properties configuration class.
 */
@ConfigurationProperties(prefix = LogConstants.LOG_PROPERTY_PREFIX)
@Getter
@Setter
@ToString
public class LoggingProperties {

    private String headerName = LogConstants.CORRELATIONID_HEADER;
    private String mdcKey = LogConstants.MDC_KEY;
    private boolean enableScheduled = Boolean.FALSE;
    private boolean enableSqsListener = Boolean.FALSE;

}
