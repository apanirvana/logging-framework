package ke.co.apollo.gis.logging.async;

import ke.co.apollo.gis.logging.config.LoggingProperties;
import ke.co.apollo.gis.logging.util.CorrelationIdUtil;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.MDC;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;

import javax.annotation.PostConstruct;

/**
 * Interceptor Around aspect for messaging listener methods.
 */
@Aspect
@ConditionalOnBean(value = LoggingProperties.class)
@Slf4j
public class MessagingListenerInterceptor extends BaseInterceptor {

    private LoggingProperties loggingProperties;

    /**
     * Parameterized constructor.
     *
     * @param loggingProperties - logging property values.
     */
    public MessagingListenerInterceptor(LoggingProperties loggingProperties) {
        super(loggingProperties);
    }

    /**
     * Initializer method.
     */
    @PostConstruct
    public void init() {
        log.info("Initialized interceptor for messaging listener.");
    }

    /**
     * Pointcut to determine methods annotated with @{org.springframework.cloud.aws.messaging.listener.annotation.SqsListener}
     */
    @Pointcut("@annotation(org.springframework.cloud.aws.messaging.listener.annotation.SqsListener)")
    public void messagingListenerMethods() {
    }

    /**
     * Around advice to handle MDC logging for SQS messaging listener methods.
     *
     * @param proceedingJoinPoint - scheduled method intercepted.
     * @throws Throwable - if error occurrs
     */
    @Around("messagingListenerMethods() && targetScheduledMethods()")
    @Override
    public void intercept(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        addMdcContext(proceedingJoinPoint);
    }

}
