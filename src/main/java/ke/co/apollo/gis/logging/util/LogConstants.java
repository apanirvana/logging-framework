package ke.co.apollo.gis.logging.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Constants class.
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class LogConstants {

    public static final String LOG_PROPERTY_PREFIX = "correlation.logging";
    public static final String CORRELATIONID_HEADER = "X-Correlation-Id";
    public static final String MDC_KEY = "RequestId";
}
