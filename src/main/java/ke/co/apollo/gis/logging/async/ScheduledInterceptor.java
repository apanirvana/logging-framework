package ke.co.apollo.gis.logging.async;

import ke.co.apollo.gis.logging.config.LoggingProperties;
import ke.co.apollo.gis.logging.util.CorrelationIdUtil;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.MDC;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;

import javax.annotation.PostConstruct;

/**
 * Interceptor Around aspect for Scheduled methods.
 */
@Aspect
@ConditionalOnBean(value = LoggingProperties.class)
@Slf4j
public class ScheduledInterceptor extends BaseInterceptor {

    private LoggingProperties loggingProperties;

    /**
     * Parameterized constructor.
     *
     * @param loggingProperties - logging property values.
     */
    public ScheduledInterceptor(LoggingProperties loggingProperties) {
        super(loggingProperties);
    }

    /**
     * Initializer method.
     */
    @PostConstruct
    public void init() {
        log.info("Initialized interceptor for scheduled jobs.");
    }

    /**
     * Pointcut to determine methods annotated with @{@link org.springframework.scheduling.annotation.Scheduled}
     */
    @Pointcut("@annotation(org.springframework.scheduling.annotation.Scheduled)")
    public void scheduledMethods() {
    }


    /**
     * Around advice to handle MDC logging for async scheduled methods.
     *
     * @param proceedingJoinPoint - scheduled method intercepted.
     * @throws Throwable - if error occurrs
     */
    @Around("scheduledMethods() && targetScheduledMethods()")
    @Override
    public void intercept(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        addMdcContext(proceedingJoinPoint);

    }
}
