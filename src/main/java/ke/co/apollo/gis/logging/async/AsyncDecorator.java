package ke.co.apollo.gis.logging.async;

import ke.co.apollo.gis.logging.config.LoggingProperties;
import ke.co.apollo.gis.logging.util.CorrelationIdUtil;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.core.task.TaskDecorator;

/**
 * Decorator to add correlation id to new thread context.
 */
@Slf4j
public class AsyncDecorator implements TaskDecorator {

    private LoggingProperties loggingProperties;

    /**
     * Parameterized constructor.
     *
     * @param loggingProperties - logging properties.
     */
    public AsyncDecorator(LoggingProperties loggingProperties) {
        this.loggingProperties = loggingProperties;
    }

    /**
     * Overridden decorate method to add context to runnable.
     *
     * @param runnable - task to execute
     * @return Runnable - new runnable wrapped with MDC.
     */
    @Override
    public Runnable decorate(Runnable runnable) {
        final String correlationId = CorrelationIdUtil
                .getCorrelationId(this.loggingProperties.getMdcKey());
        return () -> {
            try {
                MDC.put(this.loggingProperties.getMdcKey(), correlationId);
                runnable.run();
            } finally {
                MDC.clear();
            }
        };
    }
}
