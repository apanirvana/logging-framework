package ke.co.apollo.gis.logging.config;

import ke.co.apollo.gis.logging.async.AsyncDecorator;
import ke.co.apollo.gis.logging.async.MessagingListenerInterceptor;
import ke.co.apollo.gis.logging.async.ScheduledInterceptor;
import ke.co.apollo.gis.logging.filter.CorrelationFilter;
import ke.co.apollo.gis.logging.rest.RestApiInterceptor;
import ke.co.apollo.gis.logging.util.LogConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.core.task.TaskDecorator;
import org.springframework.core.task.TaskExecutor;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import javax.servlet.DispatcherType;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Optional;

/**
 * Configuration class for creating beans for intercepting requests(Http, Async, Scheduled etc). Invoked when
 * CorrelationLogging is enabled.
 */
@Configuration
@EnableAspectJAutoProxy
@Slf4j
public class CorrelationConfig implements BeanPostProcessor {

    private LoggingProperties loggingProperties;

    public CorrelationConfig(final LoggingProperties loggingProperties) {
        this.loggingProperties = loggingProperties;
    }

    @PostConstruct
    public void init() {
        log.info("Initializing Logging Framework for Correlation based logging.");
        log.info("Correlation Logging enabled with : {}", loggingProperties);
    }

    /**
     * CorrelationId Filter Bean
     *
     * @return FilterRegistrationBean
     */
    @Bean
    public FilterRegistrationBean<CorrelationFilter> correlationFilter() {
        log.info("Initializing Correlation Filter.");
        FilterRegistrationBean<CorrelationFilter> filter = new FilterRegistrationBean<>();
        filter.setFilter(new CorrelationFilter(this.loggingProperties));
        filter.setOrder(Integer.MIN_VALUE);
        filter.setAsyncSupported(true);
        filter.setDispatcherTypes(DispatcherType.REQUEST, DispatcherType.FORWARD,
                DispatcherType.ASYNC);
        return filter;
    }

    /**
     * Async Scheduled calls interceptor Bean.
     *
     * @return ScheduledInterceptor
     */
    @Bean
    @ConditionalOnProperty(value = LogConstants.LOG_PROPERTY_PREFIX + ".enableScheduled", havingValue = "true", matchIfMissing = false)
    public ScheduledInterceptor scheduledInterceptor() {
        log.info("Initializing Scheduled Interceptor AspectJ bean");
        return new ScheduledInterceptor(this.loggingProperties);
    }

    /**
     * SQS Messaging listener Aspect interceptor Bean.
     *
     * @return MessagingListenerInterceptor
     */
    @Bean
    @ConditionalOnProperty(value = LogConstants.LOG_PROPERTY_PREFIX + ".enableSqsListener", havingValue = "true", matchIfMissing = false)
    public MessagingListenerInterceptor messagingListenerInterceptor() {
        log.info("Initializing Messaging Listener Interceptor AspectJ bean");
        return new MessagingListenerInterceptor(this.loggingProperties);
    }

    /**
     * Bean post processor for initializing {@link RestTemplate} and {@link TaskExecutor}
     *
     * @param bean     - Bean instance
     * @param beanName - Name of bean
     * @return Object
     * @throws BeansException - if error in creating bean
     */
    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if (bean instanceof RestTemplate) {
            log.info("RestTemplate bean found. Configuring correlation logging interceptor.");
            RestTemplate restTemplateBean = (RestTemplate) bean;
            List<ClientHttpRequestInterceptor> interceptorList = restTemplateBean.getInterceptors();
            interceptorList.add(new RestApiInterceptor(this.loggingProperties));
            restTemplateBean.setInterceptors(interceptorList);
        } else if (TaskExecutor.class.isInstance(bean)) {
            log.info("Task Executor Bean found. Configuring task decorator for setting correlation id in context.");
            Optional<Method> method = Optional.ofNullable(ReflectionUtils.findMethod(bean.getClass(),
                    "setTaskDecorator", TaskDecorator.class));
            method.ifPresent(m -> ReflectionUtils.invokeMethod(m, bean, new AsyncDecorator(this.loggingProperties)));
            Optional.ofNullable(ReflectionUtils.findMethod(bean.getClass(), "initialize"))
                    .ifPresent(m -> ReflectionUtils.invokeMethod(m, bean));
        }
        return bean;
    }
}
