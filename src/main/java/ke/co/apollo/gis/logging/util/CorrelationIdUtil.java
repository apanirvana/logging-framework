package ke.co.apollo.gis.logging.util;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.MDC;

import java.util.UUID;

/**
 * Utility for generating random id for each execution.
 */
public class CorrelationIdUtil {

    /**
     * Generates new request id as random UUID.
     *
     * @return random uuid
     */
    public static String generateId() {
        String uuid =  UUID.randomUUID().toString();
        return StringUtils.replace(uuid, "-", StringUtils.EMPTY);
    }

    /**
     * Validate if correlationId exists or else generate new.
     *
     * @param mdcKey - mdc key to validate correlation id.
     * @return String - calculated correlation id.
     */
    public static String getCorrelationId(final String mdcKey) {
        String correlationId = MDC.get(mdcKey);
        if (StringUtils.isBlank(correlationId)) {
            correlationId = generateId();
        }
        return correlationId;
    }
}
