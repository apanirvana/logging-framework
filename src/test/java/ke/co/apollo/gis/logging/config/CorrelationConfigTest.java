package ke.co.apollo.gis.logging.config;

import ke.co.apollo.gis.logging.async.AsyncDecorator;
import ke.co.apollo.gis.logging.async.ScheduledInterceptor;
import ke.co.apollo.gis.logging.filter.CorrelationFilter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.web.client.RestTemplate;

import static org.junit.jupiter.api.Assertions.*;

class CorrelationConfigTest {

    private CorrelationConfig correlationConfig;
    @Mock
    private RestTemplate restTemplate;
    @Mock
    private ThreadPoolTaskExecutor taskExecutor;
    @Mock
    private ThreadPoolTaskScheduler taskScheduler;

    @BeforeEach
    public void setup(){
        MockitoAnnotations.initMocks(this);
        correlationConfig = new CorrelationConfig(new LoggingProperties());
    }

    @Test
    public void shouldReturnCorrelaionFilterBean(){
        FilterRegistrationBean<CorrelationFilter> filter = correlationConfig.correlationFilter();
        Assertions.assertNotNull(filter);
        Assertions.assertNotNull(filter.getFilter());
    }

    @Test
    public void shouldReturnScheduledInterceptorBean(){
        ScheduledInterceptor interceptor = correlationConfig.scheduledInterceptor();
        Assertions.assertNotNull(interceptor);
    }

    @Test
    public void shouldAddInterceptorToRestTemplateBean(){
        correlationConfig.postProcessAfterInitialization(restTemplate, "restTemplate");
        Mockito.verify(restTemplate, Mockito.times(1)).setInterceptors(Mockito.anyList());
    }

    @Test
    public void shouldAddDecoratorToTaskExecutorBean(){
        correlationConfig.postProcessAfterInitialization(taskExecutor, "taskExecutor");
        Mockito.verify(taskExecutor, Mockito.times(1)).setTaskDecorator(Mockito.any(AsyncDecorator.class));
    }

    @Test
    public void shouldNotChangeBeanOfOtherTypes(){
        correlationConfig.postProcessAfterInitialization(taskScheduler, "taskScheduler");
        Mockito.verify(taskScheduler, Mockito.times(0)).getScheduledThreadPoolExecutor();
    }

}