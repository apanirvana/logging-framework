package ke.co.apollo.gis.logging.async;

import ke.co.apollo.gis.logging.config.LoggingProperties;
import org.aspectj.lang.ProceedingJoinPoint;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;

class ScheduledInterceptorTest {

    private ScheduledInterceptor scheduledInterceptor;
    @Mock
    private ProceedingJoinPoint proceedingJoinPoint;

    @BeforeEach
    void setUp() throws Throwable {
        MockitoAnnotations.initMocks(this);
        scheduledInterceptor = new ScheduledInterceptor(new LoggingProperties());
        Mockito.when(proceedingJoinPoint.proceed()).thenReturn(new Object());
    }

    @Test
    public void shouldCallAspectInterceptCorrectly() throws Throwable {
        scheduledInterceptor.intercept(proceedingJoinPoint);
        Mockito.verify(proceedingJoinPoint, Mockito.times(1)).proceed();
        Mockito.verify(proceedingJoinPoint, Mockito.times(0)).proceed(null);
    }
}