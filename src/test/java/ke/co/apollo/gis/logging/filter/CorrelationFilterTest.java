package ke.co.apollo.gis.logging.filter;

import ke.co.apollo.gis.logging.config.LoggingProperties;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class CorrelationFilterTest {

    private CorrelationFilter correlationFilter;
    private LoggingProperties loggingProperties;
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private FilterChain chain;
    @Mock
    private FilterConfig filterConfig;

    @BeforeEach
    void setUp() throws IOException, ServletException {
        MockitoAnnotations.initMocks(this);
        this.loggingProperties = new LoggingProperties();
        correlationFilter = new CorrelationFilter(this.loggingProperties);

        Mockito.doNothing().when(chain).doFilter(request, response);
    }

    @Test
    public void shouldGenerateCorrelationIdIfNotPresentInRequestHeader() throws IOException, ServletException {
        Mockito.when(request.getHeader(Mockito.eq(this.loggingProperties.getHeaderName())))
                .thenReturn(
                        StringUtils.EMPTY);

        correlationFilter.doFilter(request, response, chain);
        Mockito.verify(chain, Mockito.times(1)).doFilter(request, response);

    }

    @Test
    public void shouldUseExistingCorrelationIdIfPresentInRequestHeader() throws IOException, ServletException {
        Mockito.when(request.getHeader(Mockito.eq(this.loggingProperties.getHeaderName())))
                .thenReturn("ABC-123");

        correlationFilter.doFilter(request, response, chain);
        Mockito.verify(chain, Mockito.times(1)).doFilter(request, response);

    }

    @Test
    public void shouldCallInitAndDestroyMethods() throws ServletException {
        correlationFilter.init(filterConfig);
        correlationFilter.destroy();
        Mockito.verify(filterConfig, Mockito.never()).getInitParameterNames();
    }
}