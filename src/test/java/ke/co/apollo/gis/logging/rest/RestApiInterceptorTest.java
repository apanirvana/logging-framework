package ke.co.apollo.gis.logging.rest;

import ke.co.apollo.gis.logging.config.LoggingProperties;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class RestApiInterceptorTest {

    private RestApiInterceptor restApiInterceptor;
    private LoggingProperties loggingProperties;
    @Mock
    private HttpRequest httpRequest;
    private byte[] body;
    @Mock
    private ClientHttpRequestExecution execution;
    @Mock
    private ClientHttpResponse response;


    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        this.loggingProperties = new LoggingProperties();
        restApiInterceptor = new RestApiInterceptor(loggingProperties);
        body = new String("{}").getBytes();

        Mockito.when(response.getHeaders()).thenReturn(new HttpHeaders());
        Mockito.when(httpRequest.getHeaders()).thenReturn(new HttpHeaders());
    }

    @Test
    public void shouldAddCorrelationIdToRestCallHttpHeader() throws IOException {
        Mockito.when(execution.execute(httpRequest, body)).thenReturn(response);
        response.getHeaders().add(this.loggingProperties.getHeaderName(), "ABC-123");
        ClientHttpResponse res = restApiInterceptor.intercept(httpRequest, body, execution);
        Assertions.assertNotNull(res);
        Assertions.assertNotNull(res.getHeaders());
        Assertions.assertNotNull(res.getHeaders().getFirst(loggingProperties.getHeaderName()));
    }

    @Test
    public void shouldAddCorrelationIdToRestCallHttpHeaderAndResponseHeaderIfNotAlreadyPresent() throws IOException {
        Mockito.when(execution.execute(httpRequest, body)).thenReturn(response);
        ClientHttpResponse res = restApiInterceptor.intercept(httpRequest, body, execution);
        Assertions.assertNotNull(res);
        Assertions.assertNotNull(res.getHeaders());
        Assertions.assertNotNull(res.getHeaders().getFirst(loggingProperties.getHeaderName()));
    }
}