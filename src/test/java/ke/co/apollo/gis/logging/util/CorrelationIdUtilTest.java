package ke.co.apollo.gis.logging.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.MDC;

class CorrelationIdUtilTest {

    @Test
    public void shouldReturnExistingCorrelationIdOrGenerateNew(){
        String correlationId1 = CorrelationIdUtil.getCorrelationId("mdcKey");
        Assertions.assertNotNull(correlationId1);
        String correlationId2 = CorrelationIdUtil.getCorrelationId("mdcKey");
        Assertions.assertNotNull(correlationId2);

        Assertions.assertNotEquals(correlationId1, correlationId2);

        MDC.put("mdcKey", correlationId1);
        Assertions.assertEquals(correlationId1, CorrelationIdUtil.getCorrelationId("mdcKey"));
        MDC.clear();

    }

}