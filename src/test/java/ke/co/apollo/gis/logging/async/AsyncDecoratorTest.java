package ke.co.apollo.gis.logging.async;

import ke.co.apollo.gis.logging.config.LoggingProperties;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class AsyncDecoratorTest {

    private LoggingProperties loggingProperties;
    private AsyncDecorator asyncDecorator;

    @BeforeEach
    public void setup(){
        this.loggingProperties = new LoggingProperties();
        asyncDecorator = new AsyncDecorator(this.loggingProperties);
    }

    @Test
    public void shouldReturnRunnableWrapperWithMDC(){
        Runnable runnable = asyncDecorator.decorate(() -> {});
        Assertions.assertNotNull(runnable);
    }


}